$fn=40;

translate([0, 0, 1]){

// face rectangle with holes punched for LCD, toggle switch and nav stick
translate([1, 1, 1.6]){
difference() { 
    cube([67,44,1]);
    translate([13.274, 33.474, 0]){
        cylinder(2, 6.25, 6.25);}
        
    translate([6.12, 13.012, 0]){
        cube([12.7, 12.7, 5]);}
        
    translate([21.602, 12.898, 0]){
        cube([41.4, 24.3, 5]);}
}

// 1mm ring on toggle switch hole to be flush on PCB
translate([13.274, 33.474, -1]){
    difference() {
        cylinder(1, 6.75, 6.75);
        cylinder(4, 6.25, 6.25);
        }
    }
}

// rectangle edge, PCB is 1.6mm and we add 1mm for solders
difference() {
    translate([0, 0, -1]){cube([69, 46, 3.6]);}
    translate([1, 1, -1]){cube([67, 44, 5]);}
}

// make space for rather large LCD border/board
translate([21.602, 12.898, 2.6]){
    difference() {
    cube([43.4, 26.3, 1.4]);
    translate([1, 1, 0]){cube([41.4, 24.3, 0.4]);}
    translate([3.7, 10.4, 0]){cube([36, 12, 10]);}
}
}
}