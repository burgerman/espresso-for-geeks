$fn=20;

difference() { 
    cube([110,75,2]); 
    translate([10, 10]) {cylinder(5, 1.25, 1.25);}
    translate([10, 10]) {cylinder(4, 2.5, 2.5);}
    
    translate([44.7, 10]) {cylinder(5, 1.25, 1.25);}
    translate([44.7, 10]) {cylinder(4, 2.5, 2.5);}
    
    translate([12.97, 68.09]) {cylinder(5, 1.25, 1.25);}
    translate([12.97, 68.09]) {cylinder(4, 2.5, 2.5);}
    
    translate([47.1, 67.37]) {cylinder(5, 1.25, 1.25);}
    translate([47.1, 67.37]) {cylinder(4, 2.5, 2.5);}
    
    translate([70, 67.37]) {cylinder(5, 1.25, 1.25);}
    translate([70, 67.37]) {cylinder(4, 2.5, 2.5);}
    
    translate([103.78, 67.37]) {cylinder(5, 1.25, 1.25);}
    translate([103.78, 67.37]) {cylinder(4, 2.5, 2.5);}
    
    translate([70, 38.6]) {cylinder(5, 1.25, 1.25);}
    translate([70, 38.6]) {cylinder(4, 2.5, 2.5);}
    
    translate([103.78, 38.6]) {cylinder(5, 1.25, 1.25);}
    translate([103.78, 38.6]) {cylinder(4, 2.5, 2.5);}
    
    translate([55, 0]) {cube([80, 30, 5]);}
}

translate([10, 10, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([44.7, 10, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([12.97, 68.09, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([47.1, 67.37, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([70, 67.37, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([103.78, 67.37, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([70, 38.6, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}

translate([103.78, 38.6, 1]) {
    difference() { 
        cylinder(6, 2.5, 2.5); 
        cylinder(6, 1.25, 1.25);
        }
}


