/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// Espresso Machine Mods
// ---------------------
// Hardware
// --------
// Boiler temperature sensor (x2)
// Brewing pressure sensor
// Flowmeter
// Solenoid control

// Software
// --------
// PID temperature control (back-flush for temperature reduction)
// pressure control (phase control)
// auto-shot clock
// auto-shot volume
// auto-flowrate on fixed shot clock and shot volume
// manual mode
// brew soft-stop
// pre-infusion

#include "mbed.h"
#include "rtos.h"
#include "Small_6.h"
#include "Small_7.h"
#include "Arial_9.h"
#include "Arial12x12.h"
#include "Arial24x23.h"
#include "stdio.h"
#include "C12832_lcd.h"
#include "string"
#include "brewcontrol.h"

#if MBED_HEAP_STATS_ENABLED || MBED_STACK_STATS_ENABLED
#include "platform/mbed_stats.h"
#endif

// This is for debugging and control without a LCD/joystick
Serial pc(USBTX, USBRX); // tx, rx

// LCD object
C12832_LCD LCD;

// Menu display thread
Thread menu_thread(osPriorityNormal, 1024);

// mutex for heap between main and menu_handler threads 
Mutex heap_mutex;

// Joystick for menu control
InterruptIn Fire(p9);	// JS_PUSH
InterruptIn Up(p30);   	// JS_UP
InterruptIn Down(p29); 	// JS_DOWN
InterruptIn Left(p28); 	// JS_LEFT
InterruptIn Right(p27);	// JS_RIGHT

// This is used to ignore double clicks
Timer js_timer;

// Timer for power saving mode
Timer power_save_timer;
bool power_save;
#define POWER_SAVE_TIMEOUT_S 600

// Main Brew Control class holding all brewing features and logic
// parameters are (in order):
// valve/zcd/pump power control (VALVE_CTRL)
// flow sensor input pin (FLOW_IN)
// ZDC detector input (ZCD_IN)
// pump phasecontrol signal (PUMP_CTRL)
// pressure sensor (PRESSURE_IN)
// side temperature sensor (TEMP_IN)
// top temperature sensor (TEMP2_IN)
// boiler control ssr (BOILER_CTRL)
BrewControl brew_control(p10, p15, p12, p13, p20, p25, p14, p26);

// Menu structures
//
// TODO portafilter size
// TODO powersaving
#define PRE_INFUSE_TIME 0
#define PRESSURE 1
#define YIELD 2
#define SHOT_TIME 3
#define TEMPERATURE 4
#define FLOW_RATE 5
#define EMPTY 6

#define L_STANDBY 0
#define L_MENU 1
#define L_MENU_SUB 2
#define L_BREW 3

#define M_BREW_SETTINGS 0
#define M_BREW_TIME MODE_TIME
#define M_BREW_YIELD MODE_YIELD
#define M_BREW_TIME_YIELD MODE_TIME_YIELD
#define M_BREW_MANUAL MODE_MANUAL
#define M_BREW_STEAM MODE_STEAM

#define NUM_BREW_MENUS 6

const char* MENU_TITLES[NUM_BREW_MENUS]= {
    "Brew Settings",
    "Brew Set Time",
    "Brew Set Yield",
    "Brew Set Time/Yield",
    "Manual Brew",
    "Steam"
};

const uint8_t MENU_ITEMS[NUM_BREW_MENUS][5]= {
    {1, 3, TEMPERATURE, PRE_INFUSE_TIME, PRESSURE},
    {1, 1, SHOT_TIME, EMPTY, EMPTY},
    {1, 1, YIELD, EMPTY, EMPTY},
    {1, 2, YIELD, SHOT_TIME, EMPTY},
    {1, 0, EMPTY, EMPTY, EMPTY}, // stub for manual brew (no sub menu)
    {1, 0, EMPTY, EMPTY, EMPTY}  // stub for steam (no sub menu)
};

const char* ITEM_STRINGS[7]= {
    "Pre-Infuse Time (s): ",
    "Pressure (bar): ",
    "Yield (ml): ",
    "Shot Time (s): ",
    "Temperature (C): ",
    "Flow rate (dl/s): ",
    ""
};

uint8_t brew_params[NUM_BREW_MENUS][3][3]= {
    { {92, 1, 9}, {0, 0, 0}, {0, 0, 0} },
    { {28, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {55, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {55, 28, 0}, {55, 28, 0}, {55, 28, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, // stub for manual brew (no sub menu)
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }  // stub for steam (no sub menu)
};

#define LEFT  static_cast<int>(1 << 0)
#define UP    static_cast<int>(1 << 1)
#define RIGHT static_cast<int>(1 << 2)
#define DOWN  static_cast<int>(1 << 3)
#define FIRE  static_cast<int>(1 << 4)

uint8_t active_menu = 0;
uint8_t menu_level = L_STANDBY;

// 0 means not in edit mode - 1 to 9 are positions on a 3x3 square
uint8_t edit_mode = 0;
const uint8_t conversion_table[9][2] = {
    {0, 0}, {1, 0}, {2, 0},
    {0, 1}, {1, 1}, {2, 1},
    {0, 2}, {1, 2}, {2, 2}
};

#define MAIN_LOOP_PERIOD_MS 500

// takes the params from given menu and give thems to brew control
void set_params(uint8_t menu_id)
{
    if (menu_id == M_BREW_MANUAL)
    {
	brew_control.set_shot_volume(0);
	brew_control.set_shot_time(0);
	brew_control.set_preinfuse_time(0);
	return;
    }
    uint8_t i;
    for(i = 0; i < MENU_ITEMS[menu_id][1]; i++) {
	switch (MENU_ITEMS[menu_id][i+2])
	{
	    case PRE_INFUSE_TIME: 
				brew_control.set_preinfuse_time(
					brew_params[menu_id][0][i]); 
				break;
	    case YIELD:		brew_control.set_shot_volume(
					brew_params[menu_id][0][i]);
				break;
	    case SHOT_TIME:	brew_control.set_shot_time(
					brew_params[menu_id][0][i]);
				break;
	    case TEMPERATURE:	brew_control.set_shot_temperature(
					brew_params[menu_id][0][i]);
				break;
	    case FLOW_RATE:	brew_control.set_shot_flow_rate(
					float(brew_params[menu_id][0][i]/10));
				break;
	    case PRESSURE:	brew_control.set_shot_pressure(
					brew_params[menu_id][0][i]);
				break;
	}
    }
}

// Draw menu
void draw_menu()
{
    uint8_t i, j;
    uint8_t state;
    string out_s;
    // clear screen if required & set
    LCD.cls();
    LCD.invert(0);

    // Draw standby and brew specific info
    if (menu_level == L_STANDBY || menu_level == L_BREW) {
	state = brew_control.get_state();

        // Draw brew screen
	LCD.set_font((unsigned char*) Small_6);
	if (menu_level == L_BREW)
	{
	    // Invert screen while brewing or steaming
	    if (state != STOPPED)
		LCD.invert(1);
	    if (active_menu == M_BREW_MANUAL)
	    {
		LCD.locate(5,16);
		LCD.printf("MANUAL");
		LCD.locate(9,23);
		LCD.printf("BREW");
	    }
	    else if (active_menu == M_BREW_TIME)
	    {
		LCD.locate(5,16);
		LCD.printf("TIME");
	    }
	    else if (active_menu == M_BREW_YIELD)
	    {
		LCD.locate(4,16);
		LCD.printf("YIELD");
	    }
	    else if (active_menu == M_BREW_TIME_YIELD)
	    {
		LCD.locate(2,16);
		LCD.printf("FLOWRATE");
	    }
	    else if (active_menu == M_BREW_STEAM)
	    {
		LCD.locate(4,16);
		LCD.printf("STEAM");
	    }
	    if (state == BREWING)
	    {
		LCD.locate(5,23);
		if (active_menu == M_BREW_STEAM)
		    LCD.printf("HOT");
		else
		    LCD.printf("BREW");
	    }
	    else if (state == PRE_INFUSING)
	    {
		LCD.locate(2,23);
		LCD.printf("PRE-INFUSE");
	    }
	    else if (state == SOFT_STOPPING)
	    {
		LCD.locate(2,23);
		LCD.printf("SOFT-STOP");
	    }
	}
        // Draw standby screen
        else
        {
            if (power_save)
            {
                LCD.locate(5,17);
                LCD.printf("POWER");
                LCD.locate(5,24);
                LCD.printf("STANDBY");
            }
            else
            {
                LCD.locate(5,19);
                LCD.printf("STANDBY");
            }
        }

        // Draw common area (metrics)
        LCD.locate(4,4);
	LCD.set_font((unsigned char*) Arial12x12);
	if (state == SOFT_STOPPING)
	    LCD.printf("Av, pressure: %.1f bar",
		    brew_control.get_average_pressure());
	else
	    LCD.printf("%.1fC %.1fbar %.1fml/s", 
		    brew_control.get_current_temperature(),
		    brew_control.get_current_pressure(),
		    brew_control.get_current_flow_rate());
	LCD.locate(55,17);
	LCD.printf("%.0f ml %.0f s",
		brew_control.get_current_volume(),
		brew_control.get_current_time());
    }

    // Draw first level menu (brew modes)
    else if (menu_level == L_MENU) {
	LCD.locate(0,12);
	LCD.set_font((unsigned char*) Arial12x12);
	LCD.printf("%s  ", MENU_TITLES[active_menu]);
    }

    // Draw second level menu (brew parameters)
    else if (menu_level == L_MENU_SUB) {
	out_s.clear();
	// Display all menu items
	for(i = 2; i < sizeof(MENU_ITEMS[active_menu])/sizeof(*MENU_ITEMS[active_menu]); i++) {
	    // Get the type of current menu item (T, P, Y or ST)
	    int idx = MENU_ITEMS[active_menu][i];
	    char default_n[8];
	    // break out if we have an empty element
	    if (idx == EMPTY)
		break;
	    out_s += ITEM_STRINGS[idx];
	    // For each menu item, get all the parameters (either 1 or 3)
	    for (j = 0; j < MENU_ITEMS[active_menu][0]; j++) {
		// Add brackets if edit selection is over current parameter
		if (edit_mode == (i-2)*3+(j+1))
		    sprintf(default_n, "[%d]", brew_params[active_menu][j][i-2]);
		else
		    sprintf(default_n, "%d", brew_params[active_menu][j][i-2]);
		out_s += default_n;
		out_s += " ";
	    }
	    out_s += '\n';
	}
        // Set position and font size based on number of parameters
	if (MENU_ITEMS[active_menu][1] == 1)
	{
	    LCD.set_font((unsigned char*) Arial12x12);
	    LCD.locate(0,12);
	}
	else if (MENU_ITEMS[active_menu][1] == 2)
	{  
	    LCD.set_font((unsigned char*) Arial12x12);
	    LCD.locate(0,6);
	}
	else
	{
	    LCD.set_font((unsigned char*) Small_7);
	    LCD.locate(0,2);
	}
	LCD.printf("%s", out_s.c_str());
    }
}

void enter_power_save()
{
    // let's disable PID temperature control
    brew_control.disable_boiler();
    // turn on LCD power saving
    //LCD.power_save(1);

    power_save = 1;
    menu_level = L_STANDBY;
}

void exit_power_save()
{
    power_save = 0;
    power_save_timer.reset();
    // enabled PID temperature control
    brew_control.enable_boiler();
    // turn off LCD power saving
    //LCD.power_save(0);
}

// should get called everytime a power saving reset event happens 
void check_power_save()
{
    if(power_save)
        exit_power_save();
    else
        power_save_timer.reset();
}

// This thread waits for joystick commands, then sets parameters and then draws the menu
void menu_handler()
{
    while(true) {
	// wait on joystick action
	uint32_t flags = ThisThread::flags_wait_any_for(LEFT | UP | RIGHT | DOWN | FIRE, 500);
 
        heap_mutex.lock();
        // Flags should return osFlagsErrorTimeout but it's returning 0...
	if (!flags)
	{
	    draw_menu();
            heap_mutex.unlock();
	    continue;
	}
        else
        {
            check_power_save();
        }

	// fire
	if (flags == FIRE) {
	    switch (menu_level)
	    {
	    case L_STANDBY:	break;
	    case L_MENU: 	break;
	    case L_MENU_SUB: 	if(!edit_mode)
				    // enter edit mode
				    edit_mode = 1;
				else
				    // exit edit mode
				    edit_mode = 0;
				break;
	    case L_BREW:	// send params to brew control b4 brewing
				if(brew_control.get_state() == STOPPED)
				{
				    // set general settings
				    set_params(M_BREW_SETTINGS);
				    // set brew mode settings
				    set_params(active_menu);
				}
				brew_control.toggle(active_menu);
				break;
	    }
	}
	// left
	else if (flags == LEFT) {
	    switch (menu_level)
	    {
	    case L_STANDBY:	break;	
	    case L_MENU: 	menu_level = L_STANDBY; break;
	    case L_MENU_SUB: 	if(!edit_mode)
				{
				    menu_level = L_MENU;
				    // if we exiting settings, let's set them
				    if(active_menu == M_BREW_SETTINGS)
					set_params(active_menu);
				}
				else
				    brew_params[active_menu]
					[conversion_table[edit_mode-1][0]]
					[conversion_table[edit_mode-1][1]]--;
				break;
	    case L_BREW:        // skip sub-menu for manual or steam mode
				if(active_menu == M_BREW_MANUAL || active_menu == M_BREW_STEAM)
				    menu_level = L_MENU;
				// only exit brew menu if not in brewing mode
				else if(brew_control.get_state() == STOPPED)
				    menu_level = L_MENU_SUB;
				break;
	    }
	}
	// up
	else if (flags == UP) {
	    switch (menu_level)
	    {
	    case L_STANDBY:	menu_level = L_MENU; break;
	    case L_MENU: 	active_menu = (!active_menu)?active_menu:active_menu-1;
				break;
	    case L_MENU_SUB: 	if(edit_mode > 1)
				    // sub 1 or 3 based on number of phases
				    edit_mode -= (MENU_ITEMS[active_menu][0] == 1)?3:1;
				break;
	    case L_BREW: 	if(active_menu == M_BREW_MANUAL)
				    brew_control.pressure_up(5);
				break;
	    }
	}
	// right
	else if (flags == RIGHT) {
	    switch (menu_level)
	    {
	    case L_STANDBY:	menu_level = L_MENU; break;	
	    case L_MENU: 	if(active_menu == M_BREW_MANUAL 
                                        || active_menu == M_BREW_STEAM)
				    // skip sub-menu for manual mode
				    menu_level = L_BREW;
				else
				    menu_level = L_MENU_SUB;
				break;
	    case L_MENU_SUB: 	if(!edit_mode && active_menu != M_BREW_SETTINGS)
				{
				    menu_level = L_BREW;
				}
				else
				    brew_params[active_menu]
					[conversion_table[edit_mode-1][0]]
					[conversion_table[edit_mode-1][1]]++;
				break;
	    case L_BREW: 	if(brew_control.get_state() == PRE_INFUSING)
				    brew_control.stop_preinfuse_now();
				break;
	    }
	}
	// down
	else if (flags == DOWN) {
	    switch (menu_level)
	    {
	    case L_STANDBY:	menu_level = L_MENU; break;
	    case L_MENU: 	active_menu = (active_menu < NUM_BREW_MENUS-1)?active_menu+1:active_menu;
				break;
	    case L_MENU_SUB: 	if(edit_mode < (MENU_ITEMS[active_menu][1]-1)*3
					&& edit_mode > 0)
				    // move fwd by 1 or 3 based on number of phases
				    edit_mode += (MENU_ITEMS[active_menu][0] == 1)?3:1;
				break;
	    case L_BREW:	if(active_menu == M_BREW_MANUAL)
				    brew_control.pressure_down(5);
				break;
	    } 
	}

	// draw next menu
	draw_menu();
        heap_mutex.unlock();
    }
}

// timer to ignore double clicks
bool check_js_timer()
{
    if(js_timer.read_ms() < 250)
    {
	js_timer.reset();
	return 0;
    }
    else
    {
	js_timer.reset();
	return 1;
    }
}

// Joystick ISRs just send signals to unblock menu thread 
void cycle_up()
{
    if(check_js_timer())
        menu_thread.flags_set(UP);
}
void cycle_down()
{
    if(check_js_timer())
        menu_thread.flags_set(DOWN);
}
void cycle_left()
{
    if(check_js_timer())
        menu_thread.flags_set(LEFT);
}
void cycle_right()
{
    if(check_js_timer())
        menu_thread.flags_set(RIGHT);
}
void fire_away()
{
    if(check_js_timer())
        menu_thread.flags_set(FIRE);
}


#if MBED_HEAP_STATS_ENABLED
void print_heap_stats()
{
    mbed_stats_heap_t heap_info;
    pc.printf("\nMemoryStats:");
    mbed_stats_heap_get( &heap_info );
    pc.printf("\n\tBytes allocated currently: %d", heap_info.current_size);
    pc.printf("\n\tMax bytes allocated at a given time: %d", heap_info.max_size);
    pc.printf("\n\tCumulative sum of bytes ever allocated: %d", heap_info.total_size);
    pc.printf("\n\tCurrent number of bytes allocated for the heap: %d", heap_info.reserved_size);
    pc.printf("\n\tCurrent number of allocations: %d", heap_info.alloc_cnt);
    pc.printf("\n\tNumber of failed allocations: %d", heap_info.alloc_fail_cnt);
}
#endif

#if MBED_STACK_STATS_ENABLED
void print_stack_stats()
{
    mbed_stats_stack_t stack_info[ 10 ];
    mbed_stats_stack_get( &stack_info[0] );
    pc.printf("\nCumulative Stack Info:");
    pc.printf("\n\tMaximum number of bytes used on the stack: %d", stack_info[0].max_size);
    pc.printf("\n\tCurrent number of bytes allocated for the stack: %d", stack_info[0].reserved_size);
    pc.printf("\n\tNumber of stacks stats accumulated in the structure: %d", stack_info[0].stack_cnt);
    
    mbed_stats_stack_get_each( stack_info, 10 );
    pc.printf("\nThread Stack Info:");
    for(int i=0;i < 10; i++) {
        if(stack_info[i].thread_id != 0) {
            pc.printf("\n\tThread: %d", i);
            pc.printf("\n\t\tThread Id: 0x%08X", stack_info[i].thread_id);
            pc.printf("\n\t\tMaximum number of bytes used on the stack: %d", stack_info[i].max_size);
            pc.printf("\n\t\tCurrent number of bytes allocated for the stack: %d", stack_info[i].reserved_size);
            pc.printf("\n\t\tNumber of stacks stats accumulated in the structure: %d", stack_info[i].stack_cnt); 
        }        
    }
}
#endif

// print the actual contrast
int main()
{
    pc.baud(115200);

    // Initialize default mode - 60ml shots
    int target_volume = 60;
    brew_control.set_shot_volume(target_volume);
    js_timer.start();

    power_save_timer.start();
    power_save = 0;

    // We want to call the menu_handler in another thread
    menu_thread.start(callback(menu_handler));

    // Attach joystick ISR callbacks
    Up.rise(&cycle_up);
    Down.rise(&cycle_down);
    Left.rise(&cycle_left);
    Right.rise(&cycle_right);
    Fire.rise(&fire_away);

    // This loop will use the main thread to get commands from ttyUSB
    // it will also display debug/status info on ttyUSB
    while(true) { 
#if 1
	if (pc.readable()) {
	    char c = pc.getc();
            heap_mutex.lock();

	    // Menu navigation from ttyUSB
	    if(c == 'i')
		cycle_up();
	    else if(c == 'j')
		cycle_left();
	    else if(c == 'k')
		cycle_down();
	    else if(c == 'l')
		cycle_right();
	    else if(c == 'm')
		fire_away();

	    // b for Manual Brew Toggle
	    else if(c == 'b')
	    {
		brew_control.set_shot_volume(0);
		brew_control.set_shot_time(0);
		brew_control.toggle(MODE_MANUAL);
		pc.printf("brew to %d\n", brew_control.get_state());
	    }

	    // q and a to control target pressure
	    else if(c == 'q')
	    {
		brew_control.pressure_up();
	    }
	    else if(c == 'a')
	    {
		brew_control.pressure_down();
	    }

	    // z for Automatic Volume Brew
	    else if(c == 'z')
	    {
		brew_control.set_shot_volume(target_volume);
		brew_control.start(MODE_YIELD);
	    }

	    // w and s to set target temperature
	    else if (c == 'w')
	    {
		brew_control.set_shot_temperature(brew_control.get_shot_temperature() + 0.5);
		pc.printf("Temp set to %.1f\n", brew_control.get_shot_temperature());
	    }
	    else if (c == 's')
	    {
		brew_control.set_shot_temperature(brew_control.get_shot_temperature() - 0.5);
		pc.printf("Temp set to %.1f\n", brew_control.get_shot_temperature());
	    }

	    // e and d to set pre-infuse time
	    else if (c == 'e')
	    {
		brew_control.set_preinfuse_time(brew_control.get_preinfuse_time() + 1);
		pc.printf("Pre-Infuse set to %d seconds\n", brew_control.get_preinfuse_time());
	    }
	    else if (c == 'd')
	    {
		brew_control.set_preinfuse_time(brew_control.get_preinfuse_time() - 1);
		pc.printf("Pre-Infuse set to %d seconds\n", brew_control.get_preinfuse_time());
	    }

	    // r and f to set target shot volume
	    else if (c == 'r')
	    {
		target_volume = brew_control.get_shot_volume() + 5;
		brew_control.set_shot_volume(target_volume);
		pc.printf("Target Volume set to %d\n", target_volume);
	    }
	    else if (c == 'f')
	    {
		target_volume = brew_control.get_shot_volume() - 5;
		brew_control.set_shot_volume(target_volume);
		pc.printf("Target Volume set to %d\n", target_volume);
	    }

	    // p to toggle PID control (if off there will be no heating at all)
	    else if (c == 'p')
	    {
		if (brew_control.toggle_boiler())
		    pc.printf("Enabling PID\n");
		else
		    pc.printf("Disabling PID\n");
	    }
	    else if (c == '=')
		brew_control.toggle_solenoid();

            heap_mutex.unlock();
	}
/*
	pc.printf("%d %.3f ml %.2f bar %.1f C %.1f C %.1f s %.2f ml/s\n", 
		brew_control.get_pump_level(),
		brew_control.get_current_volume(),
		brew_control.get_current_pressure(),
		brew_control.get_current_temperature_side(),
		brew_control.get_current_temperature_top(),
		brew_control.get_current_time(),
		brew_control.get_current_flow_rate());
                */

        pc.printf("%.1f, %.1f, %.1f, %d, %.2f, %.2f, %.2f, %d\n",
		brew_control.get_current_time(),
		brew_control.get_current_temperature_side(),
		brew_control.get_current_temperature_top(),
		brew_control.get_pump_level(),
		brew_control.get_current_pressure(),
		brew_control.get_current_volume(),
		brew_control.get_current_flow_rate(),
                brew_control.get_state());

#if MBED_HEAP_STATS_ENABLED
        print_heap_stats();
#endif

#if MBED_STACK_STATS_ENABLED
        print_stack_stats();
#endif

#endif

        heap_mutex.lock();
        // If there has been no keypress for POWER_SAVE_TIMEOUT_S, go to powersaving mode
        if (power_save_timer.read() > POWER_SAVE_TIMEOUT_S)
            enter_power_save();
        heap_mutex.unlock();

        ThisThread::sleep_for(MAIN_LOOP_PERIOD_MS);   // wait 0.1s
    }

}
