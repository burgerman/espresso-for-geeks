FROM fedora:28

RUN echo fastestmirror=1 >> /etc/dnf/dnf.conf && \
    dnf -y update &&  \
    dnf -y install make arm-none-eabi-newlib arm-none-eabi-gcc-cs-c++ arm-none-eabi-gcc-cs && \
    dnf clean all
