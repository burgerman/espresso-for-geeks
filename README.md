# Espresso for Geeks - An mbed based project to enhance your semi-automatic espresso machine

Here you will find all the code, schematics, PCB designs and diagrams of the Espresso for Geeks project.

More information about the project can be found on http://espresso-for-geeks.kalaf.net

To download and compile this project, you need to have mbed-cli installed and working as per:

https://github.com/ARMmbed/mbed-cli#installing-mbed-cli

Then you can download and compile with the following commands:

$ mbed-cli import https://gitlab.com/burgerman/espresso-for-geeks.git
$ cd espresso-for-geeks
$ mbed-cli compile

It is pre-set to the GCC_ARM toolchain and LPC1768 target.

These are the mods that will be supported in the 0.1 release:

* high precision temp sensor on boiler
* SSR on heaters
* micro-controller
* display + buttons
* fast SSR on pump
* zero-cross detect circuit
* electronic pressure meter
* OPV adjustment
* SSR on brewhead circuit/valve
* flowmeter

These are the features that will be implemented in the 0.1 release:

* PID water temperature control
* Pressure profiling
* Automatic shot volume control
* Automatic shot clock control
* Brew pre-infusion
* Steam control via display/buttons
* Auto-pressure set & grind size assistant
* Brew soft-stop (valve based machines)

The are the features that will be implemented in the 0.2 release:

* Automatic backflush
* Automatic descale
* Enhanced steam (slow water inject)

Future features:

* Water level monitoring (requires more hw mods)
* Enhanced boiler performance (no electronics) (requires more hw mods)
* PID auto-tune
* Tom’s water temperature controller
* Recipes/Flavor profiles/Rating
* Advanced visualization

Fedora setup:

```
sudo dnf install make arm-none-eabi-newlib arm-none-eabi-gcc-cs-c++ arm-none-eabi-gcc-cs
```

See http://espresso-for-geeks.kalaf.net/features/
